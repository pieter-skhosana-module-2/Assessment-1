void main() {
  //creating an app object
  var app = appInformation(
      "Ambani", "Best South African Solution", "Ambani Africa", 2021);
  //print the app info to the consoles
  print("The name of the app is: ${app.appName}");
  print("It falls within the category of ${app.category}");
  print("It was developed by ${app.developer}");
  print("It won in the MTN Business App of the Year Awards in ${app.yearWon}");
  //convert the app name to uppercase
  var convertedAppName = app.convertToCaps(app.appName);
  print("The app name in capital letters is $convertedAppName");
}

//class to hold the app details
class appInformation {
  //class properties
  String appName = "";
  String category = "";
  String developer = "";
  int yearWon = 0;
  appInformation(
      String appName, String category, String developer, int yearWon) {
    this.appName = appName;
    this.category = category;
    this.developer = developer;
    this.yearWon = yearWon;
  }
  //method to convert a string to uppercase
  String convertToCaps(String appName) {
    return this.appName = appName.toUpperCase();
  }
}
