//function to print information
void myInformation() {
  //declaring and initializing variables
  var myName = "Pieter Skhosana";
  var favoriteApp = "MTNApp";
  var favoriteCity = "Pretoria";
//print the output to the console
  print("My name is $myName.");
  print("My favorite app is $favoriteApp.");
  print("My favourite city is $favoriteCity.");
}

void main() {
  myInformation();
}
